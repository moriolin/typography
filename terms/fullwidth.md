# 全角

全角（fullwidth）は、フォントのボディの幅が文字サイズと等しい、つまりボディが正方形であることをいいます。ほとんどの日本語フォントは全角でデザインされています。

（*TODO* 図版）

## 関連項目

- [ボディ](./body.md)
- [プロポーショナルメトリクス](./proportional-metrics.md)
