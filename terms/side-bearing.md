# サイドベアリング

サイドベアリング（side bearing）は、フォントを並べたときに隣接する字形の間隔が適切になるようボディに設定されたスペースのことです。

（*TODO* 図版：和文［上下左右］と欧文［左右］）

## 関連項目

- [ボディ](./body.md)
- [字面](./face.md)
- [プロポーショナルメトリクス](./proportional-metrics.md)
