# Webタイポグラフィの基礎知識と実践

Webタイポグラフィに関するキーワードを解説、同分野における知見を蓄積・共有します。  

## 概要

近年、日本語のサイトでもWebフォントへの注目が集まるなど、Webタイポグラフィに対する関心が高まりつつあります。ただ英語を始めとする欧米圏と比較すると、そのノウハウはあまり蓄積されておらず議論も少ないのが現状です。

そんな状況を鑑み、本プロジェクトではWebの制作に関わる人がタイポグラフィに取り組む際に参照できるよう、「実践」の観点からキーワードを取り上げていきます。Webタイポグラフィの知見が、日本語で蓄積・共有できる場所になればと考えています。（記事内容やタイトル、目次などの内容は、随時更新される可能性があります）

## 目次

### 文字のかたち

- [字体](./terms/jitai.md)
- [異体字](./terms/itaiji.md)
- [字形](./terms/jikei.md)
- [書体](./terms/typeface.md)
- [フォント](./terms/font.md)
- [グリフ](./terms/glyph.md)
- [ファミリー](./terms/family.md)
- [ウェイト](./terms/weight.md)
- [イタリック体](./terms/italic.md)
- [ローマン体](./terms/roman.md)
- [セリフ体](./terms/serif.md)
- [サンセリフ体](./terms/sans-serif.md)
- [明朝体](./terms/mincho.md)
- [ゴシック体](./terms/gothic.md)
- [等幅フォント](./terms/monospaced-font.md)

### 文字を組む

- [ボディ](./terms/body.md)
- [字面](./terms/face.md)
- [サイドベアリング](./terms/side-bearing.md)
- [全角](./terms/fullwidth.md)
- [プロポーショナルメトリクス](./terms/proportional-metrics.md)
- [字間](./terms/letter-space.md)
- [ベタ組み](./terms/betagumi.md)
- [カーニング](./terms/kerning.md)
- [トラッキング](./terms/tracking.md)
- スモールキャップス
- オールキャップス
- リガチャ
- 文字サイズ
- 行間・行高
- レディング
- 行長・字詰め
- 上付き文字・下付き文字
- ハイフネーション
- ジャスティフィケーション
- 禁則処理
- インデント
- 組み方向
- ルビ
- 圏点
- 分かち書き
- 和欧混植
- 従属欧文

### Webと文字

- Adobe-Japan1
- JIS X 0213：2004
- OpenType
- OpenType機能
- TrueType
- WOFF
- EOT
- ClearType
- DirectWrite
- アンチエイリアス
- エンコーディング
- 文字コード
- 文字セット
- Unicode
- Webフォント
- サブセッティング
- FOUT
- FOIT
- lang属性